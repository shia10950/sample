﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.EmployeeModels
{
    public partial class TblItemDetails
    {
        public int Id { get; set; }
        public string Fbaid { get; set; }
        public string ItemNumber { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
