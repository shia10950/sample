﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.EmployeeModels
{
    public partial class TblAmazonLocations
    {
        public TblAmazonLocations()
        {
            TblShipmentList = new HashSet<TblShipmentList>();
        }

        public int LocId { get; set; }
        public string LocCode { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string County { get; set; }
        public string FullAddress { get; set; }

        public virtual ICollection<TblShipmentList> TblShipmentList { get; set; }
    }
}
