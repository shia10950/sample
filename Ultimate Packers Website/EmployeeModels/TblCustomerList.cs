﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.EmployeeModels
{
    public partial class TblCustomerList
    {
        public TblCustomerList()
        {
            TblShipmentList = new HashSet<TblShipmentList>();
        }

        public int CustomerId { get; set; }
        public string Title { get; set; }
        public string CustomerName { get; set; }
        public double? Cid { get; set; }
        public string Active { get; set; }
        public string ContentType { get; set; }
        public string AppCreatedBy { get; set; }
        public string AppModifiedBy { get; set; }
        public string WorkflowInstanceId { get; set; }
        public string FileType { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Created { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Urlpath { get; set; }
        public string Path { get; set; }
        public string ItemType { get; set; }
        public string EncodedAbsoluteUrl { get; set; }
        public int KnackCid { get; set; }

        public virtual ICollection<TblShipmentList> TblShipmentList { get; set; }
    }
}
