﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.EmployeeModels
{
    public partial class TblShipmentList
    {
        public TblShipmentList()
        {
            TblBoxes = new HashSet<TblBoxes>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public int? CustomerId { get; set; }
        public string FbashipmentId { get; set; }
        public int? AmazonLoc { get; set; }
        public double? QtyBoxes { get; set; }
        public double? QtyPcs { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string Status { get; set; }
        public string ContentType { get; set; }
        public string AppCreatedBy { get; set; }
        public string AppModifiedBy { get; set; }
        public string WorkflowInstanceId { get; set; }
        public string FileType { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Created { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Urlpath { get; set; }
        public string Path { get; set; }
        public string ItemType { get; set; }
        public string EncodedAbsoluteUrl { get; set; }

        public virtual TblAmazonLocations AmazonLocNavigation { get; set; }
        public virtual TblCustomerList Customer { get; set; }
        public virtual ICollection<TblBoxes> TblBoxes { get; set; }
    }
}
