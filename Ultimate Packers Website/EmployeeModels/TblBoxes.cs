﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.EmployeeModels
{
    public partial class TblBoxes
    {
        public string FbashipmentBoxId { get; set; }
        public string Title { get; set; }
        public string FbaShipmentId { get; set; }
        public DateTime? Time { get; set; }
        public string BoxTypeId { get; set; }
        public double? QtyBoxes { get; set; }
        public bool? SelectBox { get; set; }
        public string Status { get; set; }
        public string ContentType { get; set; }
        public string AppCreatedBy { get; set; }
        public string AppModifiedBy { get; set; }
        public string WorkflowInstanceId { get; set; }
        public string FileType { get; set; }
        public DateTime? Modified { get; set; }
        public DateTime? Created { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public string Urlpath { get; set; }
        public string Path { get; set; }
        public string ItemType { get; set; }
        public string EncodedAbsoluteUrl { get; set; }

        public virtual TblShipmentList FbaShipment { get; set; }
    }
}
