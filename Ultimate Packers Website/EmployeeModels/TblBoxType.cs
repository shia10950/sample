﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.EmployeeModels
{
    public partial class TblBoxType
    {
        public int BoxTypeId { get; set; }
        public string Title { get; set; }
        public string BoxName { get; set; }
    }
}
