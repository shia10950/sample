﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Ultimate_Packers_Website.EmployeeModels
{
    public partial class BoxProgramContext : DbContext
    {
        public BoxProgramContext()
        {
        }

        public BoxProgramContext(DbContextOptions<BoxProgramContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TblAmazonLocations> TblAmazonLocations { get; set; }
        public virtual DbSet<TblBoxDetails> TblBoxDetails { get; set; }
        public virtual DbSet<TblBoxType> TblBoxType { get; set; }
        public virtual DbSet<TblBoxes> TblBoxes { get; set; }
        public virtual DbSet<TblCustomerList> TblCustomerList { get; set; }
        public virtual DbSet<TblItemDetails> TblItemDetails { get; set; }
        public virtual DbSet<TblShipmentList> TblShipmentList { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("name=Employee");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<TblAmazonLocations>(entity =>
            {
                entity.HasKey(e => e.LocId);

                entity.ToTable("tblAmazonLocations");

                entity.Property(e => e.LocId).HasColumnName("LocID");

                entity.Property(e => e.Address)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.County).HasMaxLength(25);

                entity.Property(e => e.FullAddress).HasMaxLength(100);

                entity.Property(e => e.LocCode)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(2);

                entity.Property(e => e.Zip)
                    .IsRequired()
                    .HasMaxLength(10);
            });

            modelBuilder.Entity<TblBoxDetails>(entity =>
            {
                entity.ToTable("tblBoxDetails");

                entity.HasIndex(e => e.FbashipmentboxId)
                    .HasName("IX_tblBoxDetails_BOXID");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AppCreatedBy).HasMaxLength(25);

                entity.Property(e => e.AppModifiedBy).HasMaxLength(25);

                entity.Property(e => e.ContentType).HasMaxLength(25);

                entity.Property(e => e.CreatedBy).HasMaxLength(25);

                entity.Property(e => e.EncodedAbsoluteUrl)
                    .HasColumnName("EncodedAbsoluteURL")
                    .HasMaxLength(25);

                entity.Property(e => e.FbashipmentboxId)
                    .IsRequired()
                    .HasColumnName("FBAShipmentboxID")
                    .HasMaxLength(30);

                entity.Property(e => e.FileType).HasMaxLength(25);

                entity.Property(e => e.ItemNumber).HasMaxLength(15);

                entity.Property(e => e.ItemNumberType)
                    .HasColumnName("ItemNUmberType")
                    .HasMaxLength(50);

                entity.Property(e => e.ItemType).HasMaxLength(25);

                entity.Property(e => e.ModifiedBy).HasMaxLength(25);

                entity.Property(e => e.Path).HasMaxLength(25);

                entity.Property(e => e.Status)
                    .HasMaxLength(15)
                    .HasDefaultValueSql("('Open')");

                entity.Property(e => e.Title).HasMaxLength(25);

                entity.Property(e => e.Urlpath)
                    .HasColumnName("URLPath")
                    .HasMaxLength(25);

                entity.Property(e => e.WorkflowInstanceId)
                    .HasColumnName("WorkflowInstanceID")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<TblBoxType>(entity =>
            {
                entity.HasKey(e => e.BoxTypeId)
                    .HasName("PK__tblBoxTy__5D8892502C82F3AC");

                entity.ToTable("tblBoxType");

                entity.Property(e => e.BoxTypeId).HasColumnName("BoxTypeID");

                entity.Property(e => e.BoxName).HasMaxLength(25);

                entity.Property(e => e.Title).HasMaxLength(50);
            });

            modelBuilder.Entity<TblBoxes>(entity =>
            {
                entity.HasKey(e => e.FbashipmentBoxId)
                    .ForSqlServerIsClustered(false);

                entity.ToTable("tblBoxes");

                entity.HasIndex(e => e.FbaShipmentId)
                    .HasName("IX_tblBoxes_FBAID");

                entity.Property(e => e.FbashipmentBoxId)
                    .HasColumnName("FBAShipmentBoxID")
                    .HasMaxLength(30)
                    .ValueGeneratedNever();

                entity.Property(e => e.AppCreatedBy).HasMaxLength(25);

                entity.Property(e => e.AppModifiedBy).HasMaxLength(25);

                entity.Property(e => e.BoxTypeId)
                    .HasColumnName("BoxTypeID")
                    .HasMaxLength(10);

                entity.Property(e => e.ContentType).HasMaxLength(25);

                entity.Property(e => e.CreatedBy).HasMaxLength(25);

                entity.Property(e => e.EncodedAbsoluteUrl)
                    .HasColumnName("EncodedAbsoluteURL")
                    .HasMaxLength(25);

                entity.Property(e => e.FbaShipmentId)
                    .IsRequired()
                    .HasColumnName("FbaShipmentID")
                    .HasMaxLength(15);

                entity.Property(e => e.FileType).HasMaxLength(25);

                entity.Property(e => e.ItemType).HasMaxLength(25);

                entity.Property(e => e.ModifiedBy).HasMaxLength(25);

                entity.Property(e => e.Path).HasMaxLength(25);

                entity.Property(e => e.Status)
                    .HasMaxLength(15)
                    .HasDefaultValueSql("('Open')");

                entity.Property(e => e.Title).HasMaxLength(50);

                entity.Property(e => e.Urlpath)
                    .HasColumnName("URLPath")
                    .HasMaxLength(25);

                entity.Property(e => e.WorkflowInstanceId)
                    .HasColumnName("WorkflowInstanceID")
                    .HasMaxLength(25);

                entity.HasOne(d => d.FbaShipment)
                    .WithMany(p => p.TblBoxes)
                    .HasPrincipalKey(p => p.FbashipmentId)
                    .HasForeignKey(d => d.FbaShipmentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_tblBoxes_tblShipmentList");
            });

            modelBuilder.Entity<TblCustomerList>(entity =>
            {
                entity.HasKey(e => e.CustomerId)
                    .HasName("PK__tblCusto__A4AE64B88DF45589");

                entity.ToTable("tblCustomerList");

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.Active).HasMaxLength(255);

                entity.Property(e => e.AppCreatedBy).HasMaxLength(255);

                entity.Property(e => e.AppModifiedBy).HasMaxLength(255);

                entity.Property(e => e.Cid).HasColumnName("CID");

                entity.Property(e => e.ContentType).HasMaxLength(255);

                entity.Property(e => e.CreatedBy).HasMaxLength(255);

                entity.Property(e => e.CustomerName).HasMaxLength(255);

                entity.Property(e => e.EncodedAbsoluteUrl)
                    .HasColumnName("EncodedAbsoluteURL")
                    .HasMaxLength(255);

                entity.Property(e => e.FileType).HasMaxLength(255);

                entity.Property(e => e.ItemType).HasMaxLength(255);

                entity.Property(e => e.KnackCid)
                    .HasColumnName("KnackCID")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.ModifiedBy).HasMaxLength(255);

                entity.Property(e => e.Path).HasMaxLength(255);

                entity.Property(e => e.Title).HasMaxLength(255);

                entity.Property(e => e.Urlpath)
                    .HasColumnName("URLPath")
                    .HasMaxLength(255);

                entity.Property(e => e.WorkflowInstanceId)
                    .HasColumnName("WorkflowInstanceID")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<TblItemDetails>(entity =>
            {
                entity.ToTable("tblItemDetails");

                entity.HasIndex(e => e.Fbaid)
                    .HasName("IX_tblItemDetailsFBAID");

                entity.HasIndex(e => e.ItemNumber)
                    .HasName("IX_tblItemDetailsItemNum");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.ExpirationDate)
                    .HasColumnType("date")
                    .HasDefaultValueSql("(' ')");

                entity.Property(e => e.Fbaid)
                    .IsRequired()
                    .HasColumnName("FBAID")
                    .HasMaxLength(10);

                entity.Property(e => e.ItemNumber)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TblShipmentList>(entity =>
            {
                entity.ToTable("tblShipmentList");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("IX_tblShipmentList_CUSTID");

                entity.HasIndex(e => e.FbashipmentId)
                    .HasName("IX_tblShipmentList_FBAID")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Address1).HasMaxLength(55);

                entity.Property(e => e.Address2).HasMaxLength(55);

                entity.Property(e => e.AppCreatedBy).HasMaxLength(255);

                entity.Property(e => e.AppModifiedBy).HasMaxLength(255);

                entity.Property(e => e.City).HasMaxLength(25);

                entity.Property(e => e.ContentType).HasMaxLength(25);

                entity.Property(e => e.Country).HasMaxLength(15);

                entity.Property(e => e.CreatedBy).HasMaxLength(25);

                entity.Property(e => e.CustomerId).HasColumnName("CustomerID");

                entity.Property(e => e.EncodedAbsoluteUrl)
                    .HasColumnName("EncodedAbsoluteURL")
                    .HasMaxLength(25);

                entity.Property(e => e.FbashipmentId)
                    .IsRequired()
                    .HasColumnName("FBAShipmentID")
                    .HasMaxLength(15);

                entity.Property(e => e.FileType).HasMaxLength(255);

                entity.Property(e => e.ItemType).HasMaxLength(25);

                entity.Property(e => e.ModifiedBy).HasMaxLength(25);

                entity.Property(e => e.Path).HasMaxLength(25);

                entity.Property(e => e.State).HasMaxLength(15);

                entity.Property(e => e.Status)
                    .HasMaxLength(15)
                    .HasDefaultValueSql("('Open')");

                entity.Property(e => e.Title).HasMaxLength(55);

                entity.Property(e => e.Urlpath)
                    .HasColumnName("URLPath")
                    .HasMaxLength(25);

                entity.Property(e => e.WorkflowInstanceId)
                    .HasColumnName("WorkflowInstanceID")
                    .HasMaxLength(255);

                entity.Property(e => e.Zip).HasMaxLength(15);

                entity.HasOne(d => d.AmazonLocNavigation)
                    .WithMany(p => p.TblShipmentList)
                    .HasForeignKey(d => d.AmazonLoc)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_tblShipmentList_tblAmazonLocations");

                entity.HasOne(d => d.Customer)
                    .WithMany(p => p.TblShipmentList)
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("FK_tblShipmentList_tblCustomerList");
            });
        }
    }
}
