﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultimate_Packers_Website.GlobalModels;

namespace Ultimate_Packers_Website.ViewModels
{
    public class LPDetailsViewModel
    {
        public IEnumerable<TblDamagedItems> DamagedItems { get; set; }
        public IEnumerable<TblInventoryDetails> InventoryDetails { get; set; }
        public IEnumerable<TblInventoryTransaction> InventoryTransactions { get; set; }
    }
}
