﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Ultimate_Packers_Website.GlobalModels;

namespace Ultimate_Packers_Website.ViewModels
{
    public class ChangeLocationViewModel
    {
        public List<TblLocations> Locations { get; set; }
        public TblLicencePlates LicencePlates { get; set; }
    }
}
