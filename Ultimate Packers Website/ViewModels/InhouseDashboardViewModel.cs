﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultimate_Packers_Website.GlobalModels;

namespace Ultimate_Packers_Website.ViewModels
{
    public class InhouseDashboardViewModel
    {
        public IEnumerable<TblLocations> Locations { get; set; }
        public IEnumerable<TblLicencePlates> LicencePlates { get; set; }
    }
}
