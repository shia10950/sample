﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultimate_Packers_Website.EmployeeModels;

namespace Ultimate_Packers_Website.ViewModels
{
    public class OpenShipmentsViewModel
    {
        public IEnumerable<TblShipmentList> Shipments { get; set; }
        public IEnumerable<TblBoxes> Boxes { get; set; }
        public IEnumerable<TblBoxDetails> BoxDetails { get; set; }
    }
}
