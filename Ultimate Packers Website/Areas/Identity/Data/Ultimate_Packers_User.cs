﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;

namespace Ultimate_Packers_Website.Areas.Identity.Data
{
    // Add profile data for application users by adding properties to the Ultimate_Packers_User class
    public class Ultimate_Packers_User : IdentityUser
    {
    }
}
