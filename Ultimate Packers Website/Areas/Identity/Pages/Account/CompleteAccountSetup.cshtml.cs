using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Ultimate_Packers_Website.GlobalModels;
using Ultimate_Packers_Website.Services.MailKit;

namespace Ultimate_Packers_Website.Areas.Identity.Pages.Account
{
    public class CompleteAccountSetupModel : PageModel
    {
        private readonly WMSContext _context;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IEmailService _emailService;

        public CompleteAccountSetupModel(WMSContext context, 
                                        UserManager<IdentityUser> userManager, 
                                        IEmailService emailService)
        {
            _context = context;
            _userManager = userManager;
            _emailService = emailService;
        }

        [BindProperty]
        public InputModel Input { get; set; }
        public class InputModel
        {
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string userName { get; set; }

            [Required]
            public string CompanyName { get; set; }
            [DataType(DataType.PhoneNumber)]
            public string Phone { get; set; }
            [DataType(DataType.EmailAddress)]
            public string Email { get; set; }
            public string BillingAddress1 { get; set; }
            public string BillingAddress2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public int zip { get; set; }
            [Required]
            [Display(Name = "Name On Card")]
            public string NameOnCard { get; set; }

            [Required]
            [DataType(DataType.CreditCard)]
            public string CardNumber { get; set; }

            [DataType(DataType.Date)]
            [DisplayFormat(DataFormatString = "{0:MM/yyyy}", ApplyFormatInEditMode = true)]
            public DateTime Expiration { get; set; }

            public int AddressID { get; set; }
            public int CVV { get; set; }
            public bool IsDefault { get; set; } 
        }

        public IActionResult OnGet()
        {
            var user = _userManager.GetUserAsync(User);
            ViewData["usersName"] = _userManager.GetUserName(User);
            if (user == null)
            {
                return NotFound($"Unable to load details for user with ID '{_userManager.GetUserId(User)}'");
            }

            return Page();
        }

        public IActionResult OnPost()
        {
            var address = new TblAddresses
            {
                AddressLine1 = Input.BillingAddress1,
                AddressLine2 = Input.BillingAddress2,
                City = Input.City,
                State = Input.State,
                Zip = Input.zip.ToString()
            };

            var creditCard = new TblCreditCards
            {
                NameOnCard = Input.NameOnCard,
                CardNumber = Input.CardNumber,
                Expiration = Input.Expiration,
                Cvv = Input.CVV,
                IsDefault = Input.IsDefault
            };

            var client = new TblClients
            {
                FirstName = Input.FirstName,
                LastName = Input.LastName,
                CompanyName = Input.CompanyName,
                PhoneNumber = Input.Phone,
                EmailAddress = Input.Email,

                ClientBillingAddress = address,
                Cc = creditCard
            };

            if (ModelState.IsValid)
            {
                //_context.TblClients.Add(client);
                //if (Input.userName != null)
                //{
                //    var user = _userManager.GetUserAsync(User);
                //    _userManager.SetUserNameAsync(User, Input.userName);
                //}
            }
            
            _context.SaveChanges();

            return Page();
        }
    }
}