﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ultimate_Packers_Website.Areas.Identity.Data;
using Ultimate_Packers_Website.GlobalModels;

[assembly: HostingStartup(typeof(Ultimate_Packers_Website.Areas.Identity.IdentityHostingStartup))]
namespace Ultimate_Packers_Website.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
                services.AddDbContext<IdentityContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("IdentityDatabse")));

                services.AddIdentity<IdentityUser, IdentityRole>(optins =>
                {
                    optins.Tokens.EmailConfirmationTokenProvider = TokenOptions.DefaultEmailProvider;
                    optins.Tokens.PasswordResetTokenProvider = TokenOptions.DefaultAuthenticatorProvider;
                })
                .AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders();

                services.Configure<IdentityOptions>(option =>
                {
                    //Password Settings
                    option.Password.RequireDigit = false;
                    option.Password.RequireUppercase = false;
                    option.Password.RequireLowercase = false;
                    option.Password.RequireNonAlphanumeric = false;
                    option.Password.RequiredLength = 6;
                    //sign in options
                    option.SignIn.RequireConfirmedEmail = false;
                    //Lockout settings
                    option.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
                    option.Lockout.MaxFailedAccessAttempts = 5;
                    option.Lockout.AllowedForNewUsers = true;
                    //username settings
                    option.User.AllowedUserNameCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
                    option.User.RequireUniqueEmail = true;
                });
            });
        }
    }
}