﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblOoheader
    {
        public double OorderId { get; set; }
        public int ClientId { get; set; }
        public string ShipmentId { get; set; }
        public string OrderType { get; set; }
        public string StatusId { get; set; }
        public string AmazonLocationId { get; set; }
        public string Description { get; set; }
        public string ShippingAddressId { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime? DateSubmitted { get; set; }
        public DateTime? DatePrepared { get; set; }
        public DateTime? DateCompleted { get; set; }
        public DateTime? DateReadyToShip { get; set; }
        public DateTime? DatePaid { get; set; }
        public DateTime? DateAddWeight { get; set; }
        public DateTime? DateShipped { get; set; }
    }
}
