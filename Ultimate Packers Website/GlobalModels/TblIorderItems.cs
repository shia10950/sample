﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblIorderItems
    {
        public TblIorderItems()
        {
            TblItemImages = new HashSet<TblItemImages>();
        }

        public int IorderItemsId { get; set; }
        public int? IorderId { get; set; }
        public int? ItemId { get; set; }
        public int? Uomid { get; set; }
        public int? QtyPieces { get; set; }
        public int? QtyBoxes { get; set; }
        public int? PiecesPerBox { get; set; }
        public int? BoxesPerPallet { get; set; }
        public int? QtyPallets { get; set; }
        public int? TotalQty { get; set; }
        public int? QtyReceived { get; set; }
        public string Notes { get; set; }
        public DateTime? Expiration { get; set; }
        public int? DamagedItemId { get; set; }

        public virtual TblDamagedItems DamagedItem { get; set; }
        public virtual TblIoheader Iorder { get; set; }
        public virtual TblItems Item { get; set; }
        public virtual TblUom Uom { get; set; }
        public virtual ICollection<TblItemImages> TblItemImages { get; set; }
    }
}
