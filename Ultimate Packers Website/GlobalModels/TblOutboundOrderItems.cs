﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblOutboundOrderItems
    {
        public int OutboundorderIitems { get; set; }
        public int ItemId { get; set; }
        public int Qty { get; set; }
    }
}
