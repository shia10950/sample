﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class WMSContext : DbContext
    {
        public WMSContext()
        {
        }

        public WMSContext(DbContextOptions<WMSContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<TblAddresses> TblAddresses { get; set; }
        public virtual DbSet<TblCarrier> TblCarrier { get; set; }
        public virtual DbSet<TblCheckedIn> TblCheckedIn { get; set; }
        public virtual DbSet<TblClientStatus> TblClientStatus { get; set; }
        public virtual DbSet<TblClientUsers> TblClientUsers { get; set; }
        public virtual DbSet<TblClients> TblClients { get; set; }
        public virtual DbSet<TblComunication> TblComunication { get; set; }
        public virtual DbSet<TblCreditCards> TblCreditCards { get; set; }
        public virtual DbSet<TblDamagedImages> TblDamagedImages { get; set; }
        public virtual DbSet<TblDamagedItems> TblDamagedItems { get; set; }
        public virtual DbSet<TblDocumants> TblDocumants { get; set; }
        public virtual DbSet<TblImages> TblImages { get; set; }
        public virtual DbSet<TblInventoryDetails> TblInventoryDetails { get; set; }
        public virtual DbSet<TblInventoryTransaction> TblInventoryTransaction { get; set; }
        public virtual DbSet<TblIodates> TblIodates { get; set; }
        public virtual DbSet<TblIoheader> TblIoheader { get; set; }
        public virtual DbSet<TblIorderItems> TblIorderItems { get; set; }
        public virtual DbSet<TblItemImages> TblItemImages { get; set; }
        public virtual DbSet<TblItemParts> TblItemParts { get; set; }
        public virtual DbSet<TblItemUoms> TblItemUoms { get; set; }
        public virtual DbSet<TblItems> TblItems { get; set; }
        public virtual DbSet<TblJobTypes> TblJobTypes { get; set; }
        public virtual DbSet<TblLicencePlates> TblLicencePlates { get; set; }
        public virtual DbSet<TblListings> TblListings { get; set; }
        public virtual DbSet<TblLocations> TblLocations { get; set; }
        public virtual DbSet<TblMarketPlaces> TblMarketPlaces { get; set; }
        public virtual DbSet<TblOoheader> TblOoheader { get; set; }
        public virtual DbSet<TblOutboundOrderItems> TblOutboundOrderItems { get; set; }
        public virtual DbSet<TblPackageType> TblPackageType { get; set; }
        public virtual DbSet<TblPricing> TblPricing { get; set; }
        public virtual DbSet<TblShipper> TblShipper { get; set; }
        public virtual DbSet<TblStatus> TblStatus { get; set; }
        public virtual DbSet<TblSupplier> TblSupplier { get; set; }
        public virtual DbSet<TblTrackingNumbers> TblTrackingNumbers { get; set; }
        public virtual DbSet<TblUom> TblUom { get; set; }
        public virtual DbSet<TblUserRoles> TblUserRoles { get; set; }
        public virtual DbSet<TblUserStatus> TblUserStatus { get; set; }
        public virtual DbSet<TblUsers> TblUsers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("name=WMS");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.0-rtm-35687");

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.AspNetUsers)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK__AspNetUse__clien__6A50C1DA");
            });

            modelBuilder.Entity<TblAddresses>(entity =>
            {
                entity.HasKey(e => e.AddressId)
                    .HasName("PK__tblAddre__091C2A1B9BF1763D");

                entity.ToTable("tblAddresses");

                entity.Property(e => e.AddressId).HasColumnName("AddressID");

                entity.Property(e => e.AddressLine1)
                    .IsRequired()
                    .HasMaxLength(55);

                entity.Property(e => e.AddressLine2).HasMaxLength(55);

                entity.Property(e => e.AddressLine3).HasMaxLength(55);

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasMaxLength(55);

                entity.Property(e => e.Country).HasMaxLength(55);

                entity.Property(e => e.State)
                    .IsRequired()
                    .HasMaxLength(55);

                entity.Property(e => e.Zip)
                    .IsRequired()
                    .HasMaxLength(15);
            });

            modelBuilder.Entity<TblCarrier>(entity =>
            {
                entity.HasKey(e => e.CarrierId)
                    .HasName("PK__tblCarri__CB820579DE57655D");

                entity.ToTable("tblCarrier");

                entity.Property(e => e.CarrierId)
                    .HasColumnName("CarrierID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BillingAddressId).HasColumnName("BillingAddressID");

                entity.Property(e => e.CarrierEmail)
                    .IsRequired()
                    .HasMaxLength(55);

                entity.Property(e => e.CarrierName)
                    .IsRequired()
                    .HasMaxLength(55);

                entity.Property(e => e.CarrierPhone)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.CarrierType)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ShippingAddressId).HasColumnName("ShippingAddressID");

                entity.HasOne(d => d.BillingAddress)
                    .WithMany(p => p.TblCarrierBillingAddress)
                    .HasForeignKey(d => d.BillingAddressId)
                    .HasConstraintName("CarrierBillingAddressID");

                entity.HasOne(d => d.ShippingAddress)
                    .WithMany(p => p.TblCarrierShippingAddress)
                    .HasForeignKey(d => d.ShippingAddressId)
                    .HasConstraintName("CarrierShippingAddress");
            });

            modelBuilder.Entity<TblCheckedIn>(entity =>
            {
                entity.HasKey(e => e.CheckedInId)
                    .HasName("PK__tblCheck__8D4368B5C1D5253E");

                entity.ToTable("tblCheckedIn");

                entity.Property(e => e.CheckedInId)
                    .HasColumnName("CheckedInID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.IorderId).HasColumnName("IOrderID");

                entity.Property(e => e.NameOnShipment).HasMaxLength(55);

                entity.Property(e => e.Notes).HasMaxLength(250);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.TblCheckedIn)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CheckedInClientID");

                entity.HasOne(d => d.Iorder)
                    .WithMany(p => p.TblCheckedIn)
                    .HasForeignKey(d => d.IorderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CheckedInIOrderID");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TblCheckedIn)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CheckedInStatusID");
            });

            modelBuilder.Entity<TblClientStatus>(entity =>
            {
                entity.HasKey(e => e.ClieStatusId)
                    .HasName("PK__tblClien__F1212F66AF61E5AA");

                entity.ToTable("tblClientStatus");

                entity.Property(e => e.ClieStatusId)
                    .HasColumnName("ClieStatusID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientStatus)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ClientStatusType)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TblClientUsers>(entity =>
            {
                entity.HasKey(e => e.ClientUsersId)
                    .HasName("PK__tblClien__8B6596FDE8B6A2C6");

                entity.ToTable("tblClientUsers");

                entity.Property(e => e.ClientUsersId)
                    .HasColumnName("ClientUsersID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.UserId).HasColumnName("UserID");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.TblClientUsers)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ClientID");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TblClientUsers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("UserID");
            });

            modelBuilder.Entity<TblClients>(entity =>
            {
                entity.HasKey(e => e.ClientId)
                    .HasName("PK__tblClien__E67E1A041C5B62A6");

                entity.ToTable("tblClients");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.Ccid).HasColumnName("CCID");

                entity.Property(e => e.ClientBillingAddressId).HasColumnName("ClientBillingAddressID");

                entity.Property(e => e.ClientStatusId).HasColumnName("ClientStatusID");

                entity.Property(e => e.CompanyName).HasMaxLength(55);

                entity.Property(e => e.EmailAddress).HasMaxLength(255);

                entity.Property(e => e.FirstName).HasMaxLength(255);

                entity.Property(e => e.LastName).HasMaxLength(255);

                entity.Property(e => e.PhoneNumber).HasMaxLength(25);

                entity.Property(e => e.ShippingAddressId).HasColumnName("ShippingAddressID");

                entity.HasOne(d => d.Cc)
                    .WithMany(p => p.TblClients)
                    .HasForeignKey(d => d.Ccid)
                    .HasConstraintName("FK__tblClients__CCID__48EFCE0F");

                entity.HasOne(d => d.ClientBillingAddress)
                    .WithMany(p => p.TblClientsClientBillingAddress)
                    .HasForeignKey(d => d.ClientBillingAddressId)
                    .HasConstraintName("FK_tblItemsBilling_tblAddresses");

                entity.HasOne(d => d.ClientStatus)
                    .WithMany(p => p.TblClients)
                    .HasForeignKey(d => d.ClientStatusId)
                    .HasConstraintName("FK_clientStatusID_tblClientStatus");

                entity.HasOne(d => d.ShippingAddress)
                    .WithMany(p => p.TblClientsShippingAddress)
                    .HasForeignKey(d => d.ShippingAddressId)
                    .HasConstraintName("FK_tblItemsShipping_tblAddresses");
            });

            modelBuilder.Entity<TblComunication>(entity =>
            {
                entity.HasKey(e => e.ComId)
                    .HasName("PK__tblComun__E15F41329A910C1F");

                entity.ToTable("tblComunication");

                entity.Property(e => e.ComId)
                    .HasColumnName("ComID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ComText)
                    .IsRequired()
                    .HasMaxLength(255);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.Property(e => e.ToUser).HasColumnName("ToUSer");

                entity.HasOne(d => d.FromUserNavigation)
                    .WithMany(p => p.TblComunicationFromUserNavigation)
                    .HasForeignKey(d => d.FromUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ComunicationFromUser");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TblComunication)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ComunicationStatusID");

                entity.HasOne(d => d.ToUserNavigation)
                    .WithMany(p => p.TblComunicationToUserNavigation)
                    .HasForeignKey(d => d.ToUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ComunicationToUSer");
            });

            modelBuilder.Entity<TblCreditCards>(entity =>
            {
                entity.HasKey(e => e.Ccid)
                    .HasName("PK_tblCreditCard");

                entity.ToTable("tblCreditCards");

                entity.Property(e => e.Ccid).HasColumnName("CCID");

                entity.Property(e => e.AddressId).HasColumnName("AddressID");

                entity.Property(e => e.CardNumber).HasMaxLength(25);

                entity.Property(e => e.Cvv).HasColumnName("CVV");

                entity.Property(e => e.Expiration).HasColumnType("date");

                entity.Property(e => e.NameOnCard)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.Address)
                    .WithMany(p => p.TblCreditCards)
                    .HasForeignKey(d => d.AddressId)
                    .HasConstraintName("FK__CreditCar__Addre__442B18F2");
            });

            modelBuilder.Entity<TblDamagedImages>(entity =>
            {
                entity.HasKey(e => e.DamagedItemImageId);

                entity.ToTable("tblDamagedImages");

                entity.Property(e => e.DamagedItemImageId).HasColumnName("DamagedItemImageID");

                entity.Property(e => e.Image).HasMaxLength(255);

                entity.Property(e => e.ItemImageType).HasMaxLength(20);

                entity.Property(e => e.ListingId).HasColumnName("ListingID");
            });

            modelBuilder.Entity<TblDamagedItems>(entity =>
            {
                entity.HasKey(e => e.DamagedItemId)
                    .HasName("PK__tblDamag__8F744D1D39F15B50");

                entity.ToTable("tblDamagedItems");

                entity.Property(e => e.DamagedItemId).HasColumnName("DamagedItemID");

                entity.Property(e => e.DamagedItemImageId).HasColumnName("DamagedItemImageID");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.Lpid).HasColumnName("LPID");

                entity.Property(e => e.Notes).HasMaxLength(250);

                entity.Property(e => e.Uomid).HasColumnName("UOMID");

                entity.HasOne(d => d.DamagedItemImage)
                    .WithMany(p => p.TblDamagedItems)
                    .HasForeignKey(d => d.DamagedItemImageId)
                    .HasConstraintName("FK__tblDamage__Damag__2665ABE1");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.TblDamagedItems)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("FK__tblDamage__ItemI__2759D01A");

                entity.HasOne(d => d.Lp)
                    .WithMany(p => p.TblDamagedItems)
                    .HasForeignKey(d => d.Lpid)
                    .HasConstraintName("FK__tblDamaged__LPID__515009E6");

                entity.HasOne(d => d.Uom)
                    .WithMany(p => p.TblDamagedItems)
                    .HasForeignKey(d => d.Uomid)
                    .HasConstraintName("FK__tblDamage__UOMID__2942188C");
            });

            modelBuilder.Entity<TblDocumants>(entity =>
            {
                entity.HasKey(e => e.FileId)
                    .HasName("PK__tblDocum__6F0F989F38CCF288");

                entity.ToTable("tblDocumants");

                entity.Property(e => e.FileId)
                    .HasColumnName("FileID")
                    .ValueGeneratedNever();

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.FileType)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.IorderId).HasColumnName("IOrderID");

                entity.Property(e => e.TrackingId).HasColumnName("TrackingID");

                entity.HasOne(d => d.Iorder)
                    .WithMany(p => p.TblDocumants)
                    .HasForeignKey(d => d.IorderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DocumantsIOrderID");

                entity.HasOne(d => d.Tracking)
                    .WithMany(p => p.TblDocumants)
                    .HasForeignKey(d => d.TrackingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("DocumantsTrackingID");
            });

            modelBuilder.Entity<TblImages>(entity =>
            {
                entity.HasKey(e => e.ImageId)
                    .HasName("PK__tblImage__7516F4EC15F0E762");

                entity.ToTable("tblImages");

                entity.Property(e => e.ImageId)
                    .HasColumnName("ImageID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("clientID");

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(1);

                entity.Property(e => e.FileType)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.IorderId).HasColumnName("IOrderID");

                entity.Property(e => e.TrackingId).HasColumnName("TrackingID");

                entity.HasOne(d => d.Iorder)
                    .WithMany(p => p.TblImages)
                    .HasForeignKey(d => d.IorderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ImagesIOrderID");

                entity.HasOne(d => d.Tracking)
                    .WithMany(p => p.TblImages)
                    .HasForeignKey(d => d.TrackingId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ImagesToUSer");
            });

            modelBuilder.Entity<TblInventoryDetails>(entity =>
            {
                entity.HasKey(e => e.InventoryDetailsId)
                    .HasName("PK__tblInven__1CCCE018E229B363");

                entity.ToTable("tblInventoryDetails");

                entity.Property(e => e.InventoryDetailsId)
                    .HasColumnName("InventoryDetailsID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateUpdated).HasColumnType("datetime");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Lpid).HasColumnName("LPID");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.TblInventoryDetails)
                    .HasForeignKey(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__tblInvent__ItemI__505BE5AD");

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.TblInventoryDetails)
                    .HasForeignKey(d => d.LocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__tblInvent__Locat__0ABD916C");

                entity.HasOne(d => d.Lp)
                    .WithMany(p => p.TblInventoryDetails)
                    .HasForeignKey(d => d.Lpid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__tblInvento__LPID__0504B816");
            });

            modelBuilder.Entity<TblInventoryTransaction>(entity =>
            {
                entity.HasKey(e => e.InventoryTransactionId)
                    .HasName("PK__tblInven__0863F868AF8DD903");

                entity.ToTable("tblInventoryTransaction");

                entity.Property(e => e.InventoryTransactionId)
                    .HasColumnName("InventoryTransactionID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ItTimestamp)
                    .IsRequired()
                    .HasColumnName("ItTIMESTAMP")
                    .IsRowVersion();

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Lpid).HasColumnName("LPID");

                entity.Property(e => e.OrderId).HasColumnName("OrderID");

                entity.Property(e => e.OrderTypeId).HasColumnName("OrderTypeID");
            });

            modelBuilder.Entity<TblIodates>(entity =>
            {
                entity.HasKey(e => e.IorderDatesId)
                    .HasName("PK__tblIODat__16CC180A74E005BB");

                entity.ToTable("tblIODates");

                entity.Property(e => e.IorderDatesId)
                    .HasColumnName("IOrderDatesID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Dates).HasColumnType("datetime");

                entity.Property(e => e.IorderId).HasColumnName("IOrderID");

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.TblIodates)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CreatedBY");

                entity.HasOne(d => d.Iorder)
                    .WithMany(p => p.TblIodates)
                    .HasForeignKey(d => d.IorderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IOrderID");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TblIodates)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKStatusID");
            });

            modelBuilder.Entity<TblIoheader>(entity =>
            {
                entity.HasKey(e => e.IorderId)
                    .HasName("PK__tblIOHea__8001D58587C8779B");

                entity.ToTable("tblIOHeader");

                entity.Property(e => e.IorderId).HasColumnName("IOrderID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.CreateBy).HasColumnName("CreateBY");

                entity.Property(e => e.NameOnShipment).HasMaxLength(255);

                entity.Property(e => e.Notes).HasMaxLength(250);

                entity.Property(e => e.StatusId).HasColumnName("StatusID");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.TblIoheader)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("StatusID");
            });

            modelBuilder.Entity<TblIorderItems>(entity =>
            {
                entity.HasKey(e => e.IorderItemsId)
                    .HasName("PK__tblIOrde__FC1DB0C9457E3FBE");

                entity.ToTable("tblIOrderItems");

                entity.Property(e => e.IorderItemsId).HasColumnName("IOrderItemsID");

                entity.Property(e => e.DamagedItemId).HasColumnName("DamagedItemID");

                entity.Property(e => e.Expiration).HasColumnType("datetime");

                entity.Property(e => e.IorderId).HasColumnName("IOrderID");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.Notes).HasMaxLength(250);

                entity.Property(e => e.Uomid).HasColumnName("UOMID");

                entity.HasOne(d => d.DamagedItem)
                    .WithMany(p => p.TblIorderItems)
                    .HasForeignKey(d => d.DamagedItemId)
                    .HasConstraintName("FK_IOrderItems_DamagedItems");

                entity.HasOne(d => d.Iorder)
                    .WithMany(p => p.TblIorderItems)
                    .HasForeignKey(d => d.IorderId)
                    .HasConstraintName("IOrderItems_OrderID");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.TblIorderItems)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("IOrderItems_ItemID");

                entity.HasOne(d => d.Uom)
                    .WithMany(p => p.TblIorderItems)
                    .HasForeignKey(d => d.Uomid)
                    .HasConstraintName("IOrderItems_UOMID");
            });

            modelBuilder.Entity<TblItemImages>(entity =>
            {
                entity.HasKey(e => e.ItemImageId)
                    .HasName("PK__tblItemI__09AE32B74CB05C80");

                entity.ToTable("tblItemImages");

                entity.Property(e => e.ItemImageId).HasColumnName("ItemImageID");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnName("IMAGE")
                    .HasMaxLength(255);

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.ItemImageType)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ListingId).HasColumnName("ListingID");

                entity.HasOne(d => d.IorderItem)
                    .WithMany(p => p.TblItemImages)
                    .HasForeignKey(d => d.IorderItemId)
                    .HasConstraintName("FK_tblIOrderItems_tblItemImages");
            });

            modelBuilder.Entity<TblItemParts>(entity =>
            {
                entity.HasKey(e => e.ItemPartId)
                    .HasName("PK__tblItemP__37F9243A953EC4E8");

                entity.ToTable("tblItemParts");

                entity.Property(e => e.ItemPartId)
                    .HasColumnName("ItemPartID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.KitItemId).HasColumnName("KitItemID");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.TblItemParts)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("itemParts_ClientID");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.TblItemPartsItem)
                    .HasForeignKey(d => d.ItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ItemID");

                entity.HasOne(d => d.KitItem)
                    .WithMany(p => p.TblItemPartsKitItem)
                    .HasForeignKey(d => d.KitItemId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("tblItemParts_KitItemID");
            });

            modelBuilder.Entity<TblItemUoms>(entity =>
            {
                entity.HasKey(e => e.ItemUomsId)
                    .HasName("PK__tblItemU__69957F900BE6C084");

                entity.ToTable("tblItemUOMs");

                entity.Property(e => e.ItemUomsId)
                    .HasColumnName("ItemUOMsID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ItemId).HasColumnName("ItemID");

                entity.Property(e => e.Qty).HasColumnName("QTY");

                entity.Property(e => e.Uomid).HasColumnName("UOMID");

                entity.HasOne(d => d.Item)
                    .WithMany(p => p.TblItemUoms)
                    .HasForeignKey(d => d.ItemId)
                    .HasConstraintName("ItemUOMs_ItemID");

                entity.HasOne(d => d.Uom)
                    .WithMany(p => p.TblItemUoms)
                    .HasForeignKey(d => d.Uomid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ItemUOMs_UOMID");
            });

            modelBuilder.Entity<TblItems>(entity =>
            {
                entity.HasKey(e => e.ItemId)
                    .HasName("PK__tblItems__727E83EB410BEA3F");

                entity.ToTable("tblItems");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.IsKit).HasColumnName("isKit");

                entity.Property(e => e.ItemImageId).HasColumnName("ItemImageID");

                entity.Property(e => e.ItemName).HasMaxLength(255);

                entity.Property(e => e.ItemNumber).HasMaxLength(255);

                entity.Property(e => e.Sku)
                    .HasColumnName("SKU")
                    .HasMaxLength(255);

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<TblJobTypes>(entity =>
            {
                entity.HasKey(e => e.JobTypeId)
                    .HasName("PK__tblJobTy__E1F4624DED4D91E0");

                entity.ToTable("tblJobTypes");

                entity.Property(e => e.JobTypeId)
                    .HasColumnName("JobTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.JobDescription).HasMaxLength(255);

                entity.Property(e => e.JobName)
                    .IsRequired()
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<TblLicencePlates>(entity =>
            {
                entity.HasKey(e => e.Lpid)
                    .HasName("PK__tblLicen__7681EDC521BE2278");

                entity.ToTable("tblLicencePlates");

                entity.Property(e => e.Lpid)
                    .HasColumnName("LPID")
                    .ValueGeneratedNever();

                entity.Property(e => e.DateCreated).HasColumnType("datetime");

                entity.Property(e => e.LocationId).HasColumnName("LocationID");

                entity.Property(e => e.Lpname)
                    .IsRequired()
                    .HasColumnName("LPName")
                    .HasMaxLength(25);

                entity.Property(e => e.Lptype)
                    .HasColumnName("LPtype")
                    .HasMaxLength(20);

                entity.HasOne(d => d.Location)
                    .WithMany(p => p.TblLicencePlates)
                    .HasForeignKey(d => d.LocationId)
                    .HasConstraintName("FK__tblLicenc__Locat__05F8DC4F");
            });

            modelBuilder.Entity<TblListings>(entity =>
            {
                entity.HasKey(e => e.ListingId)
                    .HasName("PK__tblListi__BF3EBEF04391AEA6");

                entity.ToTable("tblListings");

                entity.Property(e => e.ListingId).HasColumnName("ListingID");

                entity.Property(e => e.Asin)
                    .HasColumnName("ASIN")
                    .HasMaxLength(25);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.Fnsku)
                    .HasColumnName("FNSKU")
                    .HasMaxLength(25);

                entity.Property(e => e.JobDescription).HasMaxLength(255);

                entity.Property(e => e.JobTypeId)
                    .HasColumnName("JobTypeID")
                    .HasMaxLength(255);

                entity.Property(e => e.ListingName).HasMaxLength(255);

                entity.Property(e => e.MarketPlaceId).HasColumnName("MarketPlaceID");

                entity.Property(e => e.MerchantSku)
                    .HasColumnName("MerchantSKU")
                    .HasMaxLength(255);

                entity.Property(e => e.PriceId).HasColumnName("PriceID");

                entity.Property(e => e.Upc)
                    .HasColumnName("UPC")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<TblLocations>(entity =>
            {
                entity.HasKey(e => e.LocationId)
                    .HasName("PK__tblLocat__E7FEA477332F82F0");

                entity.ToTable("tblLocations");

                entity.Property(e => e.LocationId)
                    .HasColumnName("LocationID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Aisle)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.Bin)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.LocationName).HasMaxLength(25);

                entity.Property(e => e.Shelf)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.Stack).HasMaxLength(5);

                entity.Property(e => e.Status)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.WarehouseId).HasColumnName("WarehouseID");
            });

            modelBuilder.Entity<TblMarketPlaces>(entity =>
            {
                entity.HasKey(e => e.MarketPlaceId)
                    .HasName("PK__tblMarke__4F15A2ABB9A1DF00");

                entity.ToTable("tblMarketPlaces");

                entity.Property(e => e.MarketPlaceId)
                    .HasColumnName("MarketPlaceID")
                    .ValueGeneratedNever();

                entity.Property(e => e.MarketPlaceName)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.WebSite)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<TblOoheader>(entity =>
            {
                entity.HasKey(e => e.OorderId);

                entity.ToTable("tblOOHeader");

                entity.Property(e => e.OorderId).HasColumnName("OOrderID");

                entity.Property(e => e.AmazonLocationId)
                    .IsRequired()
                    .HasColumnName("AmazonLocationID")
                    .HasMaxLength(10);

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.DateAddWeight).HasColumnType("datetime");

                entity.Property(e => e.DateCompleted).HasColumnType("datetime");

                entity.Property(e => e.DatePaid).HasColumnType("datetime");

                entity.Property(e => e.DatePrepared).HasColumnType("datetime");

                entity.Property(e => e.DateReadyToShip).HasColumnType("datetime");

                entity.Property(e => e.DateShipped).HasColumnType("datetime");

                entity.Property(e => e.DateSubmitted).HasColumnType("datetime");

                entity.Property(e => e.Description).HasMaxLength(255);

                entity.Property(e => e.OrderDate).HasColumnType("date");

                entity.Property(e => e.OrderType)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ShipmentId)
                    .HasColumnName("ShipmentID")
                    .HasMaxLength(25);

                entity.Property(e => e.ShippingAddressId)
                    .IsRequired()
                    .HasColumnName("ShippingAddressID")
                    .HasMaxLength(150);

                entity.Property(e => e.StatusId)
                    .IsRequired()
                    .HasColumnName("StatusID")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TblOutboundOrderItems>(entity =>
            {
                entity.HasKey(e => e.OutboundorderIitems)
                    .HasName("PK__tblOutbo__4384C2B8D2AEF1C0");

                entity.ToTable("tblOutboundOrderItems");

                entity.Property(e => e.OutboundorderIitems)
                    .HasColumnName("OutboundorderIItems")
                    .ValueGeneratedNever();

                entity.Property(e => e.ItemId).HasColumnName("ItemID");
            });

            modelBuilder.Entity<TblPackageType>(entity =>
            {
                entity.HasKey(e => e.PackageTypeId)
                    .HasName("PK__tblPacka__0557DC70DE695A0D");

                entity.ToTable("tblPackageType");

                entity.Property(e => e.PackageTypeId)
                    .HasColumnName("PackageTypeID")
                    .ValueGeneratedNever();

                entity.Property(e => e.PackageType)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UsedFor).HasMaxLength(20);
            });

            modelBuilder.Entity<TblPricing>(entity =>
            {
                entity.HasKey(e => e.PriceId)
                    .HasName("PK__tblPrici__4957584F03D99B0B");

                entity.ToTable("tblPricing");

                entity.Property(e => e.PriceId)
                    .HasColumnName("PriceID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Price).HasColumnType("money");

                entity.Property(e => e.PriceDescription)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.Property(e => e.PriceFor)
                    .IsRequired()
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<TblShipper>(entity =>
            {
                entity.HasKey(e => e.ShipperId)
                    .HasName("PK__tblShipp__1F8AFFB96E213186");

                entity.ToTable("tblShipper");

                entity.Property(e => e.ShipperId)
                    .HasColumnName("ShipperID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BillingAddressId).HasColumnName("BillingAddressID");

                entity.Property(e => e.ShipperEmail)
                    .IsRequired()
                    .HasMaxLength(55);

                entity.Property(e => e.ShipperName)
                    .IsRequired()
                    .HasMaxLength(55);

                entity.Property(e => e.ShipperPhone)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.ShipperType)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.ShippingAddressId).HasColumnName("ShippingAddressID");

                entity.HasOne(d => d.BillingAddress)
                    .WithMany(p => p.TblShipperBillingAddress)
                    .HasForeignKey(d => d.BillingAddressId)
                    .HasConstraintName("shipper_BillingAddressID");

                entity.HasOne(d => d.ShippingAddress)
                    .WithMany(p => p.TblShipperShippingAddress)
                    .HasForeignKey(d => d.ShippingAddressId)
                    .HasConstraintName("shipper_ShippingAddressID");
            });

            modelBuilder.Entity<TblStatus>(entity =>
            {
                entity.HasKey(e => e.StatusId)
                    .HasName("PK__tblStatu__C8EE20435A3FC948");

                entity.ToTable("tblStatus");

                entity.Property(e => e.StatusId)
                    .HasColumnName("StatusID")
                    .ValueGeneratedNever();

                entity.Property(e => e.StatusName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.StatusType)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UsedFor)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TblSupplier>(entity =>
            {
                entity.HasKey(e => e.SupplierId)
                    .HasName("PK__tblSuppl__4BE66694EC34B206");

                entity.ToTable("tblSupplier");

                entity.Property(e => e.SupplierId)
                    .HasColumnName("SupplierID")
                    .ValueGeneratedNever();

                entity.Property(e => e.BillingAddressId).HasColumnName("BillingAddressID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.ShippingAddressId).HasColumnName("ShippingAddressID");

                entity.Property(e => e.SupplierEmail)
                    .IsRequired()
                    .HasMaxLength(55);

                entity.Property(e => e.SupplierName)
                    .IsRequired()
                    .HasMaxLength(55);

                entity.Property(e => e.SupplierPhone)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.SupplierType)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.HasOne(d => d.BillingAddress)
                    .WithMany(p => p.TblSupplierBillingAddress)
                    .HasForeignKey(d => d.BillingAddressId)
                    .HasConstraintName("FKBillingAddressID");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.TblSupplier)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKClientID");

                entity.HasOne(d => d.ShippingAddress)
                    .WithMany(p => p.TblSupplierShippingAddress)
                    .HasForeignKey(d => d.ShippingAddressId)
                    .HasConstraintName("FKShippingAddressID");
            });

            modelBuilder.Entity<TblTrackingNumbers>(entity =>
            {
                entity.HasKey(e => e.TrackingId)
                    .HasName("PK__tblTrack__3C19EDD1DF4D59B4");

                entity.ToTable("tblTrackingNumbers");

                entity.Property(e => e.TrackingId)
                    .HasColumnName("TrackingID")
                    .ValueGeneratedNever();

                entity.Property(e => e.CarrierId).HasColumnName("CarrierID");

                entity.Property(e => e.ClientId).HasColumnName("ClientID");

                entity.Property(e => e.IorderId).HasColumnName("IOrderID");

                entity.Property(e => e.PackageTypeId).HasColumnName("PackageTypeID");

                entity.Property(e => e.TrackingNumber)
                    .IsRequired()
                    .HasMaxLength(75);

                entity.Property(e => e.TrackingType).HasMaxLength(20);

                entity.HasOne(d => d.Carrier)
                    .WithMany(p => p.TblTrackingNumbers)
                    .HasForeignKey(d => d.CarrierId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TrackingNumberCarrierID");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.TblTrackingNumbers)
                    .HasForeignKey(d => d.ClientId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TrackingNumberClientID");

                entity.HasOne(d => d.Iorder)
                    .WithMany(p => p.TblTrackingNumbers)
                    .HasForeignKey(d => d.IorderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TrackingNumberIOrderID");

                entity.HasOne(d => d.PackageType)
                    .WithMany(p => p.TblTrackingNumbers)
                    .HasForeignKey(d => d.PackageTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("PackageTypeID");
            });

            modelBuilder.Entity<TblUom>(entity =>
            {
                entity.HasKey(e => e.Uomid)
                    .HasName("PK__tblUOM__9825D9FB96E1881C");

                entity.ToTable("tblUOM");

                entity.Property(e => e.Uomid)
                    .HasColumnName("UOMID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Uomname)
                    .IsRequired()
                    .HasColumnName("UOMName")
                    .HasMaxLength(25);
            });

            modelBuilder.Entity<TblUserRoles>(entity =>
            {
                entity.HasKey(e => e.RoleId)
                    .HasName("PK__tblUserR__8AFACE3A700936BA");

                entity.ToTable("tblUserRoles");

                entity.Property(e => e.RoleId)
                    .HasColumnName("RoleID")
                    .ValueGeneratedNever();

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.RoleType)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TblUserStatus>(entity =>
            {
                entity.HasKey(e => e.UserStatusId)
                    .HasName("PK__tblUserS__A33F541AE1852293");

                entity.ToTable("tblUserStatus");

                entity.Property(e => e.UserStatusId)
                    .HasColumnName("UserStatusID")
                    .ValueGeneratedNever();

                entity.Property(e => e.UserStatus)
                    .IsRequired()
                    .HasMaxLength(20);

                entity.Property(e => e.UserStatusType)
                    .IsRequired()
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<TblUsers>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PK__tblUsers__1788CCACD104EE8A");

                entity.ToTable("tblUsers");

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .ValueGeneratedNever();

                entity.Property(e => e.Password)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.Property(e => e.RoleId).HasColumnName("RoleID");

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(25);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.TblUsers)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("RoleID");
            });
        }
    }
}
