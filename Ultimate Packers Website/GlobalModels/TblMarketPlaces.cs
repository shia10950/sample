﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblMarketPlaces
    {
        public int MarketPlaceId { get; set; }
        public string MarketPlaceName { get; set; }
        public string WebSite { get; set; }
    }
}
