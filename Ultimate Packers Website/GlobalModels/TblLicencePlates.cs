﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblLicencePlates
    {
        public TblLicencePlates()
        {
            TblDamagedItems = new HashSet<TblDamagedItems>();
            TblInventoryDetails = new HashSet<TblInventoryDetails>();
        }

        public int Lpid { get; set; }
        public string Lpname { get; set; }
        public DateTime DateCreated { get; set; }
        public int? LocationId { get; set; }
        public string Lptype { get; set; }

        public virtual TblLocations Location { get; set; }
        public virtual ICollection<TblDamagedItems> TblDamagedItems { get; set; }
        public virtual ICollection<TblInventoryDetails> TblInventoryDetails { get; set; }
    }
}
