﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblUsers
    {
        public TblUsers()
        {
            TblClientUsers = new HashSet<TblClientUsers>();
            TblComunicationFromUserNavigation = new HashSet<TblComunication>();
            TblComunicationToUserNavigation = new HashSet<TblComunication>();
            TblIodates = new HashSet<TblIodates>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int RoleId { get; set; }

        public virtual TblUserRoles Role { get; set; }
        public virtual ICollection<TblClientUsers> TblClientUsers { get; set; }
        public virtual ICollection<TblComunication> TblComunicationFromUserNavigation { get; set; }
        public virtual ICollection<TblComunication> TblComunicationToUserNavigation { get; set; }
        public virtual ICollection<TblIodates> TblIodates { get; set; }
    }
}
