﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblClientUsers
    {
        public int ClientUsersId { get; set; }
        public int UserId { get; set; }
        public int ClientId { get; set; }

        public virtual TblClients Client { get; set; }
        public virtual TblUsers User { get; set; }
    }
}
