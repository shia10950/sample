﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblInventoryTransaction
    {
        public int InventoryTransactionId { get; set; }
        public int OrderId { get; set; }
        public int OrderTypeId { get; set; }
        public int LocationId { get; set; }
        public int Lpid { get; set; }
        public int ItemId { get; set; }
        public int Qty { get; set; }
        public byte[] ItTimestamp { get; set; }
    }
}
