﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblCheckedIn
    {
        public int CheckedInId { get; set; }
        public int ClientId { get; set; }
        public int IorderId { get; set; }
        public string NameOnShipment { get; set; }
        public string Notes { get; set; }
        public int StatusId { get; set; }

        public virtual TblClients Client { get; set; }
        public virtual TblIoheader Iorder { get; set; }
        public virtual TblStatus Status { get; set; }
    }
}
