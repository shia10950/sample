﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblDamagedImages
    {
        public TblDamagedImages()
        {
            TblDamagedItems = new HashSet<TblDamagedItems>();
        }

        public int? ListingId { get; set; }
        public string ItemImageType { get; set; }
        public string Image { get; set; }
        public int DamagedItemImageId { get; set; }
        public int? DamagedItemId { get; set; }

        public virtual ICollection<TblDamagedItems> TblDamagedItems { get; set; }
    }
}
