﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblUserStatus
    {
        public int UserStatusId { get; set; }
        public string UserStatus { get; set; }
        public string UserStatusType { get; set; }
    }
}
