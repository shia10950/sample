﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblItems
    {
        public TblItems()
        {
            TblDamagedItems = new HashSet<TblDamagedItems>();
            TblInventoryDetails = new HashSet<TblInventoryDetails>();
            TblIorderItems = new HashSet<TblIorderItems>();
            TblItemPartsItem = new HashSet<TblItemParts>();
            TblItemPartsKitItem = new HashSet<TblItemParts>();
            TblItemUoms = new HashSet<TblItemUoms>();
        }

        public int ClientId { get; set; }
        public int? ItemImageId { get; set; }
        public string ItemName { get; set; }
        public string ItemNumber { get; set; }
        public string Sku { get; set; }
        public string Upc { get; set; }
        public bool? Active { get; set; }
        public bool? IsKit { get; set; }
        public int ItemId { get; set; }

        public virtual ICollection<TblDamagedItems> TblDamagedItems { get; set; }
        public virtual ICollection<TblInventoryDetails> TblInventoryDetails { get; set; }
        public virtual ICollection<TblIorderItems> TblIorderItems { get; set; }
        public virtual ICollection<TblItemParts> TblItemPartsItem { get; set; }
        public virtual ICollection<TblItemParts> TblItemPartsKitItem { get; set; }
        public virtual ICollection<TblItemUoms> TblItemUoms { get; set; }
    }
}
