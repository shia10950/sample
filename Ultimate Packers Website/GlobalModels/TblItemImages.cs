﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblItemImages
    {
        public int? ListingId { get; set; }
        public int? ItemId { get; set; }
        public string ItemImageType { get; set; }
        public string Image { get; set; }
        public int? IorderItemId { get; set; }
        public int ItemImageId { get; set; }

        public virtual TblIorderItems IorderItem { get; set; }
    }
}
