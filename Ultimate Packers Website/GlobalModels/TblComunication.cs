﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblComunication
    {
        public int ComId { get; set; }
        public int FromUser { get; set; }
        public int ToUser { get; set; }
        public string ComText { get; set; }
        public int StatusId { get; set; }

        public virtual TblUsers FromUserNavigation { get; set; }
        public virtual TblStatus Status { get; set; }
        public virtual TblUsers ToUserNavigation { get; set; }
    }
}
