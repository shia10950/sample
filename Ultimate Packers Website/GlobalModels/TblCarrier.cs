﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblCarrier
    {
        public TblCarrier()
        {
            TblTrackingNumbers = new HashSet<TblTrackingNumbers>();
        }

        public int CarrierId { get; set; }
        public string CarrierType { get; set; }
        public string CarrierName { get; set; }
        public string CarrierPhone { get; set; }
        public string CarrierEmail { get; set; }
        public int? BillingAddressId { get; set; }
        public int? ShippingAddressId { get; set; }

        public virtual TblAddresses BillingAddress { get; set; }
        public virtual TblAddresses ShippingAddress { get; set; }
        public virtual ICollection<TblTrackingNumbers> TblTrackingNumbers { get; set; }
    }
}
