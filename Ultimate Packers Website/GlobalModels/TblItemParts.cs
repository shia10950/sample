﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblItemParts
    {
        public int ItemPartId { get; set; }
        public int ClientId { get; set; }
        public int KitItemId { get; set; }
        public int ItemId { get; set; }
        public int Qty { get; set; }

        public virtual TblClients Client { get; set; }
        public virtual TblItems Item { get; set; }
        public virtual TblItems KitItem { get; set; }
    }
}
