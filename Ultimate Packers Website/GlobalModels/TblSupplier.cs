﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblSupplier
    {
        public int SupplierId { get; set; }
        public int ClientId { get; set; }
        public string SupplierType { get; set; }
        public string SupplierName { get; set; }
        public string SupplierPhone { get; set; }
        public string SupplierEmail { get; set; }
        public int? BillingAddressId { get; set; }
        public int? ShippingAddressId { get; set; }

        public virtual TblAddresses BillingAddress { get; set; }
        public virtual TblClients Client { get; set; }
        public virtual TblAddresses ShippingAddress { get; set; }
    }
}
