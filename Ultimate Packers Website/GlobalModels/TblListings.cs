﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblListings
    {
        public int ListingId { get; set; }
        public int? ClientId { get; set; }
        public int? PriceId { get; set; }
        public int? MarketPlaceId { get; set; }
        public string ListingName { get; set; }
        public string MerchantSku { get; set; }
        public string Upc { get; set; }
        public string Asin { get; set; }
        public string Fnsku { get; set; }
        public string JobTypeId { get; set; }
        public int PackOf { get; set; }
        public string JobDescription { get; set; }
        public bool Active { get; set; }
    }
}
