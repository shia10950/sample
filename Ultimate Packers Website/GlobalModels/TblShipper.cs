﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblShipper
    {
        public int ShipperId { get; set; }
        public string ShipperType { get; set; }
        public string ShipperName { get; set; }
        public string ShipperPhone { get; set; }
        public string ShipperEmail { get; set; }
        public int? BillingAddressId { get; set; }
        public int? ShippingAddressId { get; set; }

        public virtual TblAddresses BillingAddress { get; set; }
        public virtual TblAddresses ShippingAddress { get; set; }
    }
}
