﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblClientStatus
    {
        public TblClientStatus()
        {
            TblClients = new HashSet<TblClients>();
        }

        public int ClieStatusId { get; set; }
        public string ClientStatus { get; set; }
        public string ClientStatusType { get; set; }

        public virtual ICollection<TblClients> TblClients { get; set; }
    }
}
