﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblPackageType
    {
        public TblPackageType()
        {
            TblTrackingNumbers = new HashSet<TblTrackingNumbers>();
        }

        public int PackageTypeId { get; set; }
        public string PackageType { get; set; }
        public string UsedFor { get; set; }

        public virtual ICollection<TblTrackingNumbers> TblTrackingNumbers { get; set; }
    }
}
