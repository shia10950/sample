﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblClients
    {
        public TblClients()
        {
            AspNetUsers = new HashSet<AspNetUsers>();
            TblCheckedIn = new HashSet<TblCheckedIn>();
            TblClientUsers = new HashSet<TblClientUsers>();
            TblItemParts = new HashSet<TblItemParts>();
            TblSupplier = new HashSet<TblSupplier>();
            TblTrackingNumbers = new HashSet<TblTrackingNumbers>();
        }

        public int ClientId { get; set; }
        public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? ClientBillingAddressId { get; set; }
        public int? ShippingAddressId { get; set; }
        public int? ClientStatusId { get; set; }
        public int? Ccid { get; set; }

        public virtual TblCreditCards Cc { get; set; }
        public virtual TblAddresses ClientBillingAddress { get; set; }
        public virtual TblClientStatus ClientStatus { get; set; }
        public virtual TblAddresses ShippingAddress { get; set; }
        public virtual ICollection<AspNetUsers> AspNetUsers { get; set; }
        public virtual ICollection<TblCheckedIn> TblCheckedIn { get; set; }
        public virtual ICollection<TblClientUsers> TblClientUsers { get; set; }
        public virtual ICollection<TblItemParts> TblItemParts { get; set; }
        public virtual ICollection<TblSupplier> TblSupplier { get; set; }
        public virtual ICollection<TblTrackingNumbers> TblTrackingNumbers { get; set; }
    }
}
