﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblDamagedItems
    {
        public TblDamagedItems()
        {
            TblIorderItems = new HashSet<TblIorderItems>();
        }

        public int DamagedItemId { get; set; }
        public int? ItemId { get; set; }
        public int? Uomid { get; set; }
        public int? PiecesPerBox { get; set; }
        public int? QtyPieces { get; set; }
        public int? QtyBoxes { get; set; }
        public string Notes { get; set; }
        public int? DamagedItemImageId { get; set; }
        public int? Lpid { get; set; }

        public virtual TblDamagedImages DamagedItemImage { get; set; }
        public virtual TblItems Item { get; set; }
        public virtual TblLicencePlates Lp { get; set; }
        public virtual TblUom Uom { get; set; }
        public virtual ICollection<TblIorderItems> TblIorderItems { get; set; }
    }
}
