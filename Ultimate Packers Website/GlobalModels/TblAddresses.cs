﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblAddresses
    {
        public TblAddresses()
        {
            TblCarrierBillingAddress = new HashSet<TblCarrier>();
            TblCarrierShippingAddress = new HashSet<TblCarrier>();
            TblClientsClientBillingAddress = new HashSet<TblClients>();
            TblClientsShippingAddress = new HashSet<TblClients>();
            TblCreditCards = new HashSet<TblCreditCards>();
            TblShipperBillingAddress = new HashSet<TblShipper>();
            TblShipperShippingAddress = new HashSet<TblShipper>();
            TblSupplierBillingAddress = new HashSet<TblSupplier>();
            TblSupplierShippingAddress = new HashSet<TblSupplier>();
        }

        public int AddressId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }

        public virtual ICollection<TblCarrier> TblCarrierBillingAddress { get; set; }
        public virtual ICollection<TblCarrier> TblCarrierShippingAddress { get; set; }
        public virtual ICollection<TblClients> TblClientsClientBillingAddress { get; set; }
        public virtual ICollection<TblClients> TblClientsShippingAddress { get; set; }
        public virtual ICollection<TblCreditCards> TblCreditCards { get; set; }
        public virtual ICollection<TblShipper> TblShipperBillingAddress { get; set; }
        public virtual ICollection<TblShipper> TblShipperShippingAddress { get; set; }
        public virtual ICollection<TblSupplier> TblSupplierBillingAddress { get; set; }
        public virtual ICollection<TblSupplier> TblSupplierShippingAddress { get; set; }
    }
}
