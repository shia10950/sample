﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblCreditCards
    {
        public TblCreditCards()
        {
            TblClients = new HashSet<TblClients>();
        }

        public int Ccid { get; set; }
        public string NameOnCard { get; set; }
        public DateTime? Expiration { get; set; }
        public int? AddressId { get; set; }
        public int? Cvv { get; set; }
        public bool? IsDefault { get; set; }
        public string CardNumber { get; set; }

        public virtual TblAddresses Address { get; set; }
        public virtual ICollection<TblClients> TblClients { get; set; }
    }
}
