﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblUom
    {
        public TblUom()
        {
            TblDamagedItems = new HashSet<TblDamagedItems>();
            TblIorderItems = new HashSet<TblIorderItems>();
            TblItemUoms = new HashSet<TblItemUoms>();
        }

        public int Uomid { get; set; }
        public string Uomname { get; set; }
        public int Qty { get; set; }

        public virtual ICollection<TblDamagedItems> TblDamagedItems { get; set; }
        public virtual ICollection<TblIorderItems> TblIorderItems { get; set; }
        public virtual ICollection<TblItemUoms> TblItemUoms { get; set; }
    }
}
