﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblItemUoms
    {
        public int ItemUomsId { get; set; }
        public int Uomid { get; set; }
        public int Qty { get; set; }
        public int? ItemId { get; set; }

        public virtual TblItems Item { get; set; }
        public virtual TblUom Uom { get; set; }
    }
}
