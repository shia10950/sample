﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblInventoryDetails
    {
        public int InventoryDetailsId { get; set; }
        public int LocationId { get; set; }
        public int Lpid { get; set; }
        public int ItemId { get; set; }
        public int QtyOnHand { get; set; }
        public int QtyAlocated { get; set; }
        public DateTime DateUpdated { get; set; }

        public virtual TblItems Item { get; set; }
        public virtual TblLocations Location { get; set; }
        public virtual TblLicencePlates Lp { get; set; }
    }
}
