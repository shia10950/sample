﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblStatus
    {
        public TblStatus()
        {
            TblCheckedIn = new HashSet<TblCheckedIn>();
            TblComunication = new HashSet<TblComunication>();
            TblIodates = new HashSet<TblIodates>();
            TblIoheader = new HashSet<TblIoheader>();
        }

        public int StatusId { get; set; }
        public string StatusName { get; set; }
        public string StatusType { get; set; }
        public string UsedFor { get; set; }

        public virtual ICollection<TblCheckedIn> TblCheckedIn { get; set; }
        public virtual ICollection<TblComunication> TblComunication { get; set; }
        public virtual ICollection<TblIodates> TblIodates { get; set; }
        public virtual ICollection<TblIoheader> TblIoheader { get; set; }
    }
}
