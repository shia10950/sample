﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblTrackingNumbers
    {
        public TblTrackingNumbers()
        {
            TblDocumants = new HashSet<TblDocumants>();
            TblImages = new HashSet<TblImages>();
        }

        public int TrackingId { get; set; }
        public int IorderId { get; set; }
        public int ClientId { get; set; }
        public int CarrierId { get; set; }
        public int PackageTypeId { get; set; }
        public string TrackingNumber { get; set; }
        public string TrackingType { get; set; }
        public int Qty { get; set; }

        public virtual TblCarrier Carrier { get; set; }
        public virtual TblClients Client { get; set; }
        public virtual TblIoheader Iorder { get; set; }
        public virtual TblPackageType PackageType { get; set; }
        public virtual ICollection<TblDocumants> TblDocumants { get; set; }
        public virtual ICollection<TblImages> TblImages { get; set; }
    }
}
