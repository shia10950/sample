﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblDocumants
    {
        public int FileId { get; set; }
        public int IorderId { get; set; }
        public int TrackingId { get; set; }
        public string FileType { get; set; }
        public byte[] FileName { get; set; }

        public virtual TblIoheader Iorder { get; set; }
        public virtual TblTrackingNumbers Tracking { get; set; }
    }
}
