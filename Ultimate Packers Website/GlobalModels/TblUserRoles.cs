﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblUserRoles
    {
        public TblUserRoles()
        {
            TblUsers = new HashSet<TblUsers>();
        }

        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public string RoleType { get; set; }

        public virtual ICollection<TblUsers> TblUsers { get; set; }
    }
}
