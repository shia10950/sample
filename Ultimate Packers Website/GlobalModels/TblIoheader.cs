﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblIoheader
    {
        public TblIoheader()
        {
            TblCheckedIn = new HashSet<TblCheckedIn>();
            TblDocumants = new HashSet<TblDocumants>();
            TblImages = new HashSet<TblImages>();
            TblIodates = new HashSet<TblIodates>();
            TblIorderItems = new HashSet<TblIorderItems>();
            TblTrackingNumbers = new HashSet<TblTrackingNumbers>();
        }

        public int IorderId { get; set; }
        public int ClientId { get; set; }
        public string NameOnShipment { get; set; }
        public string Notes { get; set; }
        public bool ReceivingPhotoRequest { get; set; }
        public bool RequestPaperWork { get; set; }
        public int StatusId { get; set; }
        public int CreateBy { get; set; }

        public virtual TblStatus Status { get; set; }
        public virtual ICollection<TblCheckedIn> TblCheckedIn { get; set; }
        public virtual ICollection<TblDocumants> TblDocumants { get; set; }
        public virtual ICollection<TblImages> TblImages { get; set; }
        public virtual ICollection<TblIodates> TblIodates { get; set; }
        public virtual ICollection<TblIorderItems> TblIorderItems { get; set; }
        public virtual ICollection<TblTrackingNumbers> TblTrackingNumbers { get; set; }
    }
}
