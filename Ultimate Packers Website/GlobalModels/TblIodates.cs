﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblIodates
    {
        public int IorderDatesId { get; set; }
        public int IorderId { get; set; }
        public int OrderType { get; set; }
        public DateTime? Dates { get; set; }
        public int StatusId { get; set; }
        public int CreatedBy { get; set; }

        public virtual TblUsers CreatedByNavigation { get; set; }
        public virtual TblIoheader Iorder { get; set; }
        public virtual TblStatus Status { get; set; }
    }
}
