﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblPricing
    {
        public int PriceId { get; set; }
        public string PriceFor { get; set; }
        public string PriceDescription { get; set; }
        public decimal Price { get; set; }
    }
}
