﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblJobTypes
    {
        public int JobTypeId { get; set; }
        public string JobName { get; set; }
        public string JobDescription { get; set; }
    }
}
