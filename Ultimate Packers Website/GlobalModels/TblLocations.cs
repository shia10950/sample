﻿using System;
using System.Collections.Generic;

namespace Ultimate_Packers_Website.GlobalModels
{
    public partial class TblLocations
    {
        public TblLocations()
        {
            TblInventoryDetails = new HashSet<TblInventoryDetails>();
            TblLicencePlates = new HashSet<TblLicencePlates>();
        }

        public int LocationId { get; set; }
        public string LocationName { get; set; }
        public int? WarehouseId { get; set; }
        public string Aisle { get; set; }
        public string Stack { get; set; }
        public string Shelf { get; set; }
        public string Bin { get; set; }
        public string Status { get; set; }

        public virtual ICollection<TblInventoryDetails> TblInventoryDetails { get; set; }
        public virtual ICollection<TblLicencePlates> TblLicencePlates { get; set; }
    }
}
