﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultimate_Packers_Website.GlobalModels;

namespace Ultimate_Packers_Website.ViewComponents
{
    public class kitComponent : ViewComponent
    {
        private readonly WMSContext _context;

        public kitComponent(WMSContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(string itemNumber, int ClientID)
        {
            var itemDetails = _context.TblItems.FirstOrDefault(i => i.ItemNumber == itemNumber);
            var clientDetails = _context.TblClients.FirstOrDefault(c => c.ClientId == ClientID);

            return View("");
        }
    }
}
