﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultimate_Packers_Website.GlobalModels;
using Ultimate_Packers_Website.ViewModels;

namespace Ultimate_Packers_Website.ViewComponents.DamagedItems
{
    public class InventoryDetailsViewComponent : ViewComponent
    {
        private readonly WMSContext _context;

        public InventoryDetailsViewComponent(WMSContext context) => _context = context;

        public async Task<IViewComponentResult> InvokeAsync(int id, string type)
        {
            if (type == "Damaged")
            {
                var damaged = await _context.TblDamagedItems.Where(i => i.Lpid == id).ToListAsync();
                var viewModel = new LPDetailsViewModel { DamagedItems = damaged };

                return View("Damaged", viewModel);
            }
            else if (type == "Inventory")
            {
                var inventory = _context.TblInventoryDetails.Where(i => i.Lpid == id).ToList();
                var viewModel = new LPDetailsViewModel { InventoryDetails = inventory };

                return View("Inventory", viewModel);
            }

            return View("NotFound");
        }
    }
}
