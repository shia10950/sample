﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultimate_Packers_Website.GlobalModels;

namespace Ultimate_Packers_Website.ViewComponents.InventoryDetails
{
    public class AdminViewModel
    {
        public IEnumerable<TblLicencePlates> licencePlates { get; set; }
        public IEnumerable<TblLocations> locations { get; set; }
    }
}
