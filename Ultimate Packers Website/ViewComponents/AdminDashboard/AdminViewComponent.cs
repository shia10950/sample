﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ultimate_Packers_Website.GlobalModels;
using Ultimate_Packers_Website.ViewModels;

namespace Ultimate_Packers_Website.ViewComponents.AdminDashboard
{
    public class AdminViewComponent : ViewComponent
    {
        private readonly WMSContext _context;

        public AdminViewComponent(WMSContext context)
        {
            _context = context;
        }

        public async Task<IViewComponentResult> InvokeAsync(string Component)
        {
            if (Component == "Locations")
            {
                var list = _context.TblLocations.ToList();
                var viewModel = new AdminViewModel { Locations = list };

                return View("Locations", viewModel);
            }

            else if (Component == "LicensePlates")
            {
                var list = _context.TblLicencePlates.ToList();
                var viewModel = new AdminViewModel { LicencePlates = list };

                return View("LicensePlates", viewModel);
            }
            
            return View("NotFound");
        }
    }
}
