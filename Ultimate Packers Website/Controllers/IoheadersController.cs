﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ultimate_Packers_Website.GlobalModels;
using Ultimate_Packers_Website.Services.MailKit;
using Ultimate_Packers_Website.ViewModels;

namespace Ultimate_Packers_Website.Controllers
{
    public class IoheadersController : Controller
    {
        private readonly WMSContext _context;
        private readonly IEmailService _emailService;
        private readonly UserManager<IdentityUser> _usermanager;

        public IoheadersController(WMSContext context, UserManager<IdentityUser> userManager, IEmailService emailService)
        {
            _context = context;
            _usermanager = userManager;
            _emailService = emailService;
        }

        // GET: Ioheaders
        public async Task<IActionResult> Index()
        {
            var wMSContext = _context.TblIoheader.Include(t => t.Status);
            return View(await wMSContext.ToListAsync());
        }

        // GET: Ioheaders/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblIoheader = await _context.TblIoheader
                .Include(t => t.Status)
                .FirstOrDefaultAsync(m => m.IorderId == id);
            if (tblIoheader == null)
            {
                return NotFound();
            }

            return View(tblIoheader);
        }

        // GET: Ioheaders/Create
        public IActionResult Create()
        {
            var lastId = _context.TblIoheader.Select(i => i.IorderId).Last();
            var id = lastId + 1;

            TempData["CreatedID"] = "IO-" + id;
            ViewData["StatusId"] = new SelectList(_context.TblStatus, "StatusId", "StatusName");
            ViewData["Items"] = new SelectList(_context.TblItems, "ItemID", "IorderItemID");

            return View();
        }

        // POST: Ioheaders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create(
        //                                [Bind("IorderId,ClientId,NameOnShipment,Notes,ReceivingPhotoRequest,RequestPaperWork,StatusId,CreateBy")]
        //                                    CreateIO createIO)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _context.Add(createIO);
        //        //_context.TblIorderItems.Add();
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }

        //    ViewData["StatusId"] = new SelectList(_context.TblStatus, "StatusId", "StatusName", createIO.Ioheader.StatusId);
        //    return View(createIO);
        //}

        // GET: Ioheaders/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblIoheader = await _context.TblIoheader.FindAsync(id);
            if (tblIoheader == null)
            {
                return NotFound();
            }
            ViewData["StatusId"] = new SelectList(_context.TblStatus, "StatusId", "StatusName", tblIoheader.StatusId);
            return View(tblIoheader);
        }

        // POST: Ioheaders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("IorderId,ClientId,NameOnShipment,Notes,ReceivingPhotoRequest,RequestPaperWork,StatusId,CreateBy")] TblIoheader tblIoheader)
        {
            if (id != tblIoheader.IorderId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblIoheader);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblIoheaderExists(tblIoheader.IorderId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StatusId"] = new SelectList(_context.TblStatus, "StatusId", "StatusName", tblIoheader.StatusId);
            return View(tblIoheader);
        }

        // GET: Ioheaders/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblIoheader = await _context.TblIoheader
                .Include(t => t.Status)
                .FirstOrDefaultAsync(m => m.IorderId == id);
            if (tblIoheader == null)
            {
                return NotFound();
            }

            return View(tblIoheader);
        }

        // POST: Ioheaders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblIoheader = await _context.TblIoheader.FindAsync(id);
            _context.TblIoheader.Remove(tblIoheader);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblIoheaderExists(int id)
        {
            return _context.TblIoheader.Any(e => e.IorderId == id);
        }
    }
}
