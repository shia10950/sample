﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ultimate_Packers_Website.EmployeeModels;
using Ultimate_Packers_Website.GlobalModels;
using Ultimate_Packers_Website.ViewModels;

namespace Ultimate_Packers_Website.Controllers
{
    public class AdminController : Controller
    {
        private readonly WMSContext _wmsContext;
        private readonly BoxProgramContext _boxProgramContext;

        public AdminController(WMSContext wMSContext, BoxProgramContext boxProgramContext)
        {
            _wmsContext = wMSContext; _boxProgramContext = boxProgramContext;
        }

        //GET Admin/Dashboard
        public IActionResult Dashboard()
        {
            return View();
        }

        //GET Admin/LocationDetails/7
        public IActionResult LocationDetails(int LocationID)
        {
            var details = _wmsContext.TblLicencePlates.Where(i => i.LocationId == LocationID).ToList();
            ViewData["ID"] = LocationID;

            return View(details);
        }

        //GET Admin/LPDetails/4
        public async Task<IActionResult> LPDetails(int id)
        {
            var damagedItems = _wmsContext.TblDamagedItems.Where(i => i.Lpid == id).Include(t => t.Item).ToList();
            var Inventory = _wmsContext.TblInventoryDetails.Where(i => i.Lpid == id).Include(t => t.Item).ToList();
            var history = _wmsContext.TblInventoryTransaction.Where(i => i.Lpid == id).ToList();

            ViewData["id"] = id;

            var viewModel = new LPDetailsViewModel { DamagedItems = damagedItems, InventoryDetails = Inventory, InventoryTransactions = history };

            return View(viewModel);
        }

        //GET Admin/ChangeLocation/54
        [HttpGet]
        public async Task<IActionResult> ChangeLocation(int id)
        {
            ViewData["id"] = id;
            var locations = _wmsContext.TblLocations.ToList();

            var LPs = _wmsContext.TblLicencePlates
                .Where(i => i.Lpid == id)
                .Include(i => i.Location)
                .FirstOrDefault();
            
            var viewModel = new ChangeLocationViewModel
            {
                LicencePlates = LPs,
                Locations = locations
            };

            return PartialView("~/Views/Shared/Partials/ChangeLocation.cshtml", viewModel);
        }

        [HttpPost]
        //[Route("Admin/ChangeLocation/{changeLocation}")]
        public async Task<IActionResult> ChangeLocation(ChangeLocationViewModel changeLocation)
        {
            var lp = _wmsContext.TblLicencePlates.Where(i => i.Lpid == changeLocation.LicencePlates.Lpid).FirstOrDefault();

            _wmsContext.TblLicencePlates.Update(lp);
            _wmsContext.SaveChanges();

            return RedirectToAction(nameof(LPDetails));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteLocation(int LocationId)
        {
            if (LocationId == 0)
            {
                return NotFound();
            }

            var location = await _wmsContext.TblLocations.FindAsync(LocationId);
            _wmsContext.TblLocations.Remove(location);
            await _wmsContext.SaveChangesAsync();

            return RedirectToAction(nameof(Dashboard));
        }
    }
}
