﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Ultimate_Packers_Website.EmployeeModels;
using Ultimate_Packers_Website.Services.MailKit;
using Ultimate_Packers_Website.services;
using Ultimate_Packers_Website.ViewModels;

namespace Ultimate_Packers_Website.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly BoxProgramContext _context;
        private readonly IEmailService _emailService;

        public EmployeesController(BoxProgramContext context, IEmailService emailService)
        {
            _context = context;
            _emailService = emailService;
        }


        //GET Admin/Customers
        public async Task<IActionResult> Customers()
        {

            var customers = _context.TblCustomerList.ToListAsync();

            return View(await customers);
        }

        //GET Employees/Shipments
        [Route("Employees/Shipments/{CustomerId}")]
        public IActionResult OpenShipments(int CustomerId)
        {
            ViewData["CustomerId"] = _context.TblCustomerList.Where(i => i.CustomerId == CustomerId).Select(i => i.CustomerId).SingleOrDefault();
            ViewData["CustomerName"] = _context.TblCustomerList.Where(i => i.CustomerId == CustomerId).Select(n => n.CustomerName).SingleOrDefault();

            var shipments = _context.TblShipmentList.Where(i => i.CustomerId == CustomerId && i.Status == "open").ToList();
            var viewModel = new OpenShipmentsViewModel { Shipments = shipments };

            return View(viewModel);
        }

        [HttpPost] 
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddShippment(TblShipmentList shippment)
        {
            if (ModelState.IsValid)
            {
                _context.TblShipmentList.Add(shippment);
                await _context.SaveChangesAsync();
            }

            return View("OpenShippments", new { id = shippment.CustomerId });
        }


        //GET Employees/OpenBoxes/ShippmentID
        [Route("Employees/OpenBoxes/{ShippmentID}")]
        public IActionResult OpenBoxes(string shippmentID, int? customerID)
        {
            var boxes = _context.TblBoxes.Where(s => s.Status == "open" && s.FbaShipmentId == shippmentID).ToList();
            var viewModel = new OpenShipmentsViewModel { Boxes = boxes };

            ViewData["ShippmentID"] = shippmentID;
            ViewData["customerID"] = customerID;

            return View(viewModel);
        }

        //Box details
        public IActionResult BoxDetails(string FbashipmentBoxId)
        {
            var info = _context.TblBoxDetails
                .Where(i => i.FbashipmentboxId == FbashipmentBoxId)
                .SingleOrDefaultAsync();

            return PartialView("~/Views/Shared/_BoxDetails", info);
        }

        //Add box
        [HttpGet]
        public async Task<IActionResult> AddBox(string FbaShipmentId)
        {
            return ViewComponent("AddBox", FbaShipmentId);
        }

        //POST AddBox
        //[ValidateAntiForgeryToken]
        //[HttpPost]
        //public IActionResult AddBox(TblBoxes box)
        //{
        //    _context.TblBoxes.Add(box);
        //    _context.SaveChanges();

        //    RedirectToAction("OpenBoxes", box.FbaShipment.FbashipmentId);
        //}
    }
}