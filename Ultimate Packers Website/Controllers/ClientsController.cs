﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ultimate_Packers_Website.GlobalModels;
using Ultimate_Packers_Website.services;
using Ultimate_Packers_Website.ViewModels;

namespace Ultimate_Packers_Website
{
    public class ClientsController : Controller
    {
        private readonly WMSContext _context;
        private readonly IEmailSender _emailSender;

        public ClientsController(WMSContext context, IEmailSender emailSender)
        {
            _context = context;
            _emailSender = emailSender;
        }

        // GET: Clients
        [Authorize]
        public async Task<IActionResult> Index()
        {
            var clients = _context.TblClients.Include(status => status.ClientStatus).ToListAsync();

            return View(await clients);
        }

        // GET: Clients/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = _context.TblClients.FirstOrDefault(c => c.ClientId == id);
            var BAdrr = _context.TblAddresses.FirstOrDefault(a => a.AddressId == client.ClientBillingAddressId);
            var SHAddr = _context.TblAddresses.FirstOrDefault(s => s.AddressId == client.ShippingAddressId);
            ViewData["ID"] = client.ClientId;

            ClientDetailsVM clientDetails = new ClientDetailsVM();
            clientDetails.LinkToImage = "";
            clientDetails.CompanyName = client.CompanyName;
            clientDetails.ClientName = client.FirstName +" "+client.LastName;
            clientDetails.BillingAddressLine1 = BAdrr.AddressLine1;
            clientDetails.BillingAddressLine2 = BAdrr.AddressLine2;
            clientDetails.BillingCity = BAdrr.City;
            clientDetails.BillingState = BAdrr.State;
            clientDetails.BillingZip = BAdrr.Zip;
            clientDetails.BillingCountry = BAdrr.Country;
            clientDetails.Phone = client.PhoneNumber;
            clientDetails.Email = client.EmailAddress;
            clientDetails.ShippingAddressLine1 = SHAddr.AddressLine1;
            clientDetails.ShippingAddressLine2 = SHAddr.AddressLine2;
            clientDetails.ShippingCity = SHAddr.City;
            clientDetails.ShippingState = SHAddr.State;
            clientDetails.ShippingZip = SHAddr.Zip;
            clientDetails.ShippingCountry = SHAddr.Country;

            if (clientDetails == null)
            {
                return NotFound();
            }

            return View(clientDetails);
        }

        // GET: Clients/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ClientId,CompanyName,PhoneNumber,EmailAddress,FirstName,LastName,ClientBillingAddressId,ShippingAddressId,ClientStatusId")] TblClients tblClients)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblClients);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(tblClients);
        }

        // GET: Clients/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblClients = await _context.TblClients.FindAsync(id);
            if (tblClients == null)
            {
                return NotFound();
            }
            return View(tblClients);
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ClientId,CompanyName,PhoneNumber,EmailAddress,FirstName,LastName,ClientBillingAddressId,ShippingAddressId,ClientStatusId")] TblClients tblClients)
        {
            if (id != tblClients.ClientId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblClients);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblClientsExists(tblClients.ClientId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(tblClients);
        }

        // GET: Clients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblClients = await _context.TblClients
                .FirstOrDefaultAsync(m => m.ClientId == id);
            if (tblClients == null)
            {
                return NotFound();
            }

            return View(tblClients);
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblClients = await _context.TblClients.FindAsync(id);
            _context.TblClients.Remove(tblClients);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblClientsExists(int id)
        {
            return _context.TblClients.Any(e => e.ClientId == id);
        }
    }
}
