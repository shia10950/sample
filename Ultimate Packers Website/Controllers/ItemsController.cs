﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Ultimate_Packers_Website.GlobalModels;

namespace Ultimate_Packers_Website.Controllers
{
    public class ItemsController : Controller
    {
        private readonly WMSContext _context;

        public ItemsController(WMSContext context)
        {
            _context = context;
        }

        // GET: Items
        public async Task<IActionResult> Index()
        {
            var wMSContext = _context.TblItems;//.Include(t => t.Client);.Include(t => t.ItemImageId);

            return View(await wMSContext.ToListAsync());
        }

        // GET: Items/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblItems = await _context.TblItems
                //.Include(t => t.Client)
                //.Include(t => t.ItemImageId)
                .FirstOrDefaultAsync(m => m.ItemId == id);
            if (tblItems == null)
            {
                return NotFound();
            }

            return View(tblItems);
        }

        // GET: Items/Create
        public IActionResult Create()
        {
            ViewData["ClientId"] = new SelectList(_context.TblClients, "ClientId", "ClientId");
            ViewData["ItemImageId"] = new SelectList(_context.TblItemImages, "ItemImageId", "ItemImageType");
            return View();
        }

        // POST: Items/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ItemId,ClientId,ItemImageId,ItemName,ItemNumber,Sku,Upc,Active")] TblItems tblItems)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tblItems);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.TblClients, "ClientId", "ClientId", tblItems.ClientId);
            ViewData["ItemImageId"] = new SelectList(_context.TblItemImages, "ItemImageId", "ItemImageType", tblItems.ItemImageId);
            return View(tblItems);
        }

        // GET: Items/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblItems = await _context.TblItems.FindAsync(id);
            if (tblItems == null)
            {
                return NotFound();
            }
            ViewData["ClientId"] = new SelectList(_context.TblClients, "ClientId", "ClientId", tblItems.ClientId);
            ViewData["ItemImageId"] = new SelectList(_context.TblItemImages, "ItemImageId", "ItemImageType", tblItems.ItemImageId);
            return View(tblItems);
        }

        // POST: Items/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ItemId,ClientId,ItemImageId,ItemName,ItemNumber,Sku,Upc,Active")] TblItems tblItems)
        {
            if (id != tblItems.ItemId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tblItems);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TblItemsExists(tblItems.ItemId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClientId"] = new SelectList(_context.TblClients, "ClientId", "ClientId", tblItems.ClientId);
            ViewData["ItemImageId"] = new SelectList(_context.TblItemImages, "ItemImageId", "ItemImageType", tblItems.ItemImageId);
            return View(tblItems);
        }

        // GET: Items/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tblItems = await _context.TblItems
                //.Include(t => t.Client)
                .Include(t => t.ItemImageId)
                .FirstOrDefaultAsync(m => m.ItemId == id);
            if (tblItems == null)
            {
                return NotFound();
            }

            return View(tblItems);
        }

        // POST: Items/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tblItems = await _context.TblItems.FindAsync(id);
            _context.TblItems.Remove(tblItems);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TblItemsExists(int id)
        {
            return _context.TblItems.Any(e => e.ItemId == id);
        }
    }
}
