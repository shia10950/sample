﻿using MailKit.Net.Pop3;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity.UI.Services;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ultimate_Packers_Website.Services.MailKit
{
    public interface IEmailService
    {
        void Send(EmailMessage emailMessage);
        List<EmailMessage> RecieveEmail(int maxCount = 10);
    }

    public class EmailService : IEmailService
    {
        private readonly IEmailConfiguration _emailConfiguration;

        //ctor
        public EmailService(IEmailConfiguration emailConfiguration)
        {
            _emailConfiguration = emailConfiguration;
        }

       
        public void Send(EmailMessage emailMessage)
        {
            var message = new MimeMessage();
            message.To.Add(new MailboxAddress(emailMessage.ToName, emailMessage.ToAddress));
            message.From.Add(new MailboxAddress(emailMessage.FromName, "Shia10950@yahoo.com"));

            message.Subject = emailMessage.Subject;
            message.Body = new TextPart(TextFormat.Html)
            {
                Text = emailMessage.Content
            };

            using (var emailClient = new SmtpClient())
            {
                emailClient.Connect("smtp.gmail.com", 465, true);
                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");
                emailClient.Authenticate("shia10950", "12553240");
                emailClient.Send(message);
                emailClient.Disconnect(true);
            }
        }

        public List<EmailMessage> RecieveEmail(int maxCount = 10)
        {
            throw new NotImplementedException();
        }

    }
}
