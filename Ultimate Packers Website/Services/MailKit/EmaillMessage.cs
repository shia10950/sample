﻿using System.Collections.Generic;
using Ultimate_Packers_Website.Services.MailKit;

public class EmailMessage
{
    public string ToName { get; set; }
    public string ToAddress { get; set; }
    public string FromName { get; set; }
    public string FromAddress { get; set; }
    public string Subject { get; set; }
    public string Content { get; set; }
    public byte[] Attatcment { get; set; }

    public List<EmailAddress> ListToEmailAddresses = new List<EmailAddress>();
    public List<EmailAddress> ListFromEmailAddresses = new List<EmailAddress>();
}