﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ultimate_Packers_Website.Services
{
    public class EmailSenderOptions
    {
        public string SendGridUser = Environment.GetEnvironmentVariable("SendGridUser");
        public string SendGridkey = Environment.GetEnvironmentVariable("SendGridkey");
    }
}
